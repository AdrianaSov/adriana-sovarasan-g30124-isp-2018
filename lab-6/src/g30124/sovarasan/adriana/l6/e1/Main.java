package g30124.sovarasan.adriana.l6.e1;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
    	 DrawingBoard b1;
         b1 = new DrawingBoard();
         
         Shape s1 = new Rectangle(Color.YELLOW, 100,50,150,100,"01",false);
         b1.addShape(s1);
         Shape s2 = new Rectangle(Color.GREEN, 100,50,150,150,"02",true);
         b1.addShape(s2);
         Shape s3=new Circle(Color.RED,100,150,200,"03",true);
         b1.addShape(s3);
    }
}
