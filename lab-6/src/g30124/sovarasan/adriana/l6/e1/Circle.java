package g30124.sovarasan.adriana.l6.e1;

import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, int radius,int x, int y, String id, boolean isFilled) {
        super(color, x, y, id, isFilled);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
    	System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(super.getX(),super.getY(),radius,radius);
       
        if(super.isFilled() == true)
            g.fillOval(super.getX(),super.getY(),radius,radius);

    }
}
