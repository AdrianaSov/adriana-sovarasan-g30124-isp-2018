package g30124.sovarasan.adriana.l6.e1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x,y;
    private String id;
    private boolean isFilled;
    
    
public Shape(Color color,int x,int y,String id,boolean isFilled) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.isFilled = isFilled;
    }

public boolean isFilled() {
    return isFilled;
	}

public Color getColor() {
    return color;
	}

public void setColor(Color color) {
    this.color = color;
	}


public int getX() {
    return x;
	}

public int getY() {
    return y;
	}

public String getId() {
    return id;
	}

public abstract void draw(Graphics g);
}