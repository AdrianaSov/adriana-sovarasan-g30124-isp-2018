package g30124.sovarasan.adriana.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int width;

    public Rectangle(Color color, int length, int width,int x,int y,String id,boolean isFilled) {
        super(color,x,y,id,isFilled);
        this.length = length;
        this.width = width;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle with length "+ length +" "+"and width " + width + " " +
    getColor().toString());
        g.setColor(getColor());
        g.drawRect(super.getX(),super.getY(),length,width);
        if(super.isFilled()==true)
            g.fillRect(super.getX(),super.getY(),length,width);
    }

}
