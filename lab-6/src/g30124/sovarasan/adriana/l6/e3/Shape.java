package g30124.sovarasan.adriana.l6.e3;

import java.awt.*;

public  interface  Shape {
    void draw(Graphics g);
    String getId();
	Color getColor();
	int getX();
	int getY();
	boolean isFilled();
}