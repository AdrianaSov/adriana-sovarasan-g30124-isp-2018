package g30124.sovarasan.adriana.l6.e3;

import java.awt.*;


public class Circle implements Shape{

    private int radius,x,y;
    private String id;
    boolean isFilled;
    private Color color;

    public Circle(Color color, int radius, int x, int y, String id, boolean isFilled) {
        this.color=color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.isFilled=isFilled;
        this.radius = radius;
    }
    
    @Override
    public int getX() {
    	return x;
    }
    
    @Override
    public int getY() {
    	return y;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    @Override 
    public Color getColor() {
    	return color;
    }
    
    @Override 
    public boolean isFilled() {
    	return isFilled;
    }
    
    @Override
    public void draw(Graphics g) {
    	System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
       
        if(isFilled() == true)
            g.fillOval(getX(),getY(),radius,radius);

    }
}
