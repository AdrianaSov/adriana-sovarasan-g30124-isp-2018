package g30124.sovarasan.adriana.l6.e4;


import static java.lang.System.in;

public class CharSequence implements java.lang.CharSequence {
    private char[] ch;

    public CharSequence(char[] ch) {
        this.ch = ch;
    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public char charAt(int index) {
        return ch[index];
    }

    @Override
    public java.lang.CharSequence subSequence(int start, int end) {
        return null;
    }
}

