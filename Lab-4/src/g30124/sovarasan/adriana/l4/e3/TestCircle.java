package g30124.sovarasan.adriana.l4.e3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestCircle {
	@Test
    public void shouldPrintArea(){
        Circle c = new Circle(10);
        
        assertEquals(314.159, c.getArea(),0.001);
 }
  @Test
    public void shouldPrintArea2(){
        Circle c = new Circle();
        
        assertEquals(3.1415, c.getArea(),0.001);
 }

}
