package g30124.sovarasan.adriana.l4.e6;


import g30124.sovarasan.adriana.l4.e4.Author;
public class Book {
	private String name;
	private Author[] author;
	private double price;
	private int qtyInStock=0;
	
	public Book(String name, Author[] author, double price) {
		this.name=name;
		this.author=author;
		this.price=price;
	}
	
	public Book(String name, Author[] author, double price,int qtyInStock) {
		this.name=name;
		this.author=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	
	public String getName() {
		return name;
	}
	
	public Author[] getAuthor() {
		return author;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice() {
		this.price=price;
	}
	
	public int getQtyInStock() {
		return qtyInStock;
	}
	
	public void setQtyInStock() {
		this.qtyInStock=qtyInStock;
	}
	
	public String toString() {
		return "book name "+name+" by "+author.length;
	}
	
	public void printAuthors() {
		for(Author a:author)
			System.out.println(a.getN());
	}

}
