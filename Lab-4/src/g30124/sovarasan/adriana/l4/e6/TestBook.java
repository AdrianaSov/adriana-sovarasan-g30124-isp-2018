package g30124.sovarasan.adriana.l4.e6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import g30124.sovarasan.adriana.l4.e4.Author;

public class TestBook {
	@Test
	public void testToString() {
		Author[] a = new Author[2];
		a[0]=new Author("a0","a0@yahoo.com",'m');
				
		a[1]=new Author("a1","a1@yahoo.com",'f');
		Book b = new Book("b1",a,10,3);
		
		assertEquals("book name b1 by 2",b.toString());
		
	}
	
	@Test
	public void testPrintAuthors() {
		Author[] a = new Author[2];
		a[0]=new Author("a0","a0@yahoo.com",'m');
				
		a[1]=new Author("a1","a1@yahoo.com",'f');
		Book b1 = new Book("b1",a,10,3);
		b1.printAuthors();
		assertEquals("a0",a[0].getN());
		assertEquals("a1",a[1].getN());
		
		
	}

}
