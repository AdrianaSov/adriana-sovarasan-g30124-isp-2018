package g30124.sovarasan.adriana.l4.e4;

public class Author {
	
	private String name,email;
	private char gender;
	
	public Author(String name, String email, char gender)
	{
		this.name=name;
		this.email=email;
		this.gender=gender;
		
	}
	
	public String getN() {
		return name;
	}
	
	public String getE() {
		return email;
	}
	
	public char getG() {
		return gender;
	}
	
	
	public void setE() {
		this.email=email;
	}
	
	public String toString() {
		return "Author "+name+" ( "+gender+" ) at email "+email;
	}
}
