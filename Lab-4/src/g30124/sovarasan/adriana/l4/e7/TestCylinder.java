package g30124.sovarasan.adriana.l4.e7;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import g30124.sovarasan.adriana.l4.e3.Circle;

public class TestCylinder {
	
	@Test
	public void testGetHeight() {
		Cylinder c1 = new Cylinder();
		Cylinder c2 = new Cylinder(10.2);
		Cylinder c3 = new Cylinder(5.0,8.0);
		
		assertEquals(1.0,c1.getHeight(),0.01);
		assertEquals(10,c2.getHeight(),0.01);
		assertEquals(8.0,c3.getHeight(),0.01);
		
	}
	
	@Test
	public void testGetVolume() {
		Cylinder c = new Cylinder(5.0,8.0);
		assertEquals(628.318,c.getVolume(),0.01);
	}
}
