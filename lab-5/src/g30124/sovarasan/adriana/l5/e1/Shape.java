package g30124.sovarasan.adriana.l5.e1;

public abstract class Shape {
    protected String color;
    protected  boolean filled;

    public Shape(){
        color = "Red";
        filled = false;
    }

    public Shape(String color, boolean filled){
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }
    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public double getArea(){
        return 0;
    }

    public double getPerimetter(){
        return 0;
    }

    @Override
    public String toString() {
        return "Shape with color " + this.color + " and filled: " +filled;
    }
}