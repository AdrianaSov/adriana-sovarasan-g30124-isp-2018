package g30124.sovarasan.adriana.l5.e2;

public class ProxyImage implements Image{

    private String fileName;
    private Image onRoleImage;

    public ProxyImage(String fileName, boolean choice){
        this.fileName = fileName;
        if(choice == true) this.onRoleImage = new RotatedImage("img1");
        else this.onRoleImage = new RealImage("img2");
    }

    @Override
    public void display() {

        onRoleImage.display();
    }
}