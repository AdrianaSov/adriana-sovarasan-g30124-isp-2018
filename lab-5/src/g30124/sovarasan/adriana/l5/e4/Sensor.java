package g30124.sovarasan.adriana.l5.e4;

public abstract class Sensor {
    protected String location;

    public Sensor(String location) {
        this.location = location;
    }

    public int readValue(){
        return 0;
    }

    public String getLocation() {
        return location;
    }

}
