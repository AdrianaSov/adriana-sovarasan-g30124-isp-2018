package g30124.sovarasan.adriana.l7.e4;

public class Main {
	
	public static void main(String[] args) {
		Dictionary d = new Dictionary();
		Word cuv = new Word("verde");
		Definition def = new Definition("culoare");		
		Word cuv1 = new Word("Iphone");
		Definition def1 = new Definition("telefon");	
		Word cuv2 = new Word("BMW");
		Definition def2 = new Definition("masina");	
		d.addWord(cuv,def);
		d.addWord(cuv1,def1);
		d.addWord(cuv2,def2);
		
		d.getAllDefinition();
		d.getDefinition(cuv);
		d.getAllWords();
	}
}