package g30124.sovarasan.adriana.l7.e2;

public class Main {
	
	public static void main(String[] args) {
		Bank bank = new Bank();
		bank.addAccount("Sam", 120.90);
		bank.addAccount("Emanuel", 100.97);
		bank.addAccount("Diana", 209.88);
		bank.addAccount("Dan", 123.55);
		
		bank.printAccounts();
		bank.printAccounts(51,120);
		bank.getAccount("Dan");
		bank.getAllAccounts();
	}

}
