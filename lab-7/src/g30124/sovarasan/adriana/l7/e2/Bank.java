package g30124.sovarasan.adriana.l7.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


class Bank{
   private ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();
    
    public void addAccount(String owner,double balance)
	{
    	accounts.add(new BankAccount(owner,balance));
	}
    
	public void printAccounts()
	{
		System.out.println("Conturile sortate: ");
		Collections.sort(accounts);
		Iterator<BankAccount> iterator = accounts.iterator();
	    while (iterator.hasNext()) {
	    	BankAccount a = iterator.next();
	    	System.out.println(a.getOwner() + " " + a.getBalance());
	    }
	}
	
	public void printAccounts(double minBalance,double maxBalance)
	{
		Iterator<BankAccount> iterator = accounts.iterator();
		   while (iterator.hasNext()) {
			   BankAccount a = iterator.next();
			   if(minBalance<a.getBalance() && a.getBalance()<maxBalance)
				   System.out.println(" \n " +a.getOwner()+ " " +a.getBalance());
			   		   
		   }
           
	}
	
	public BankAccount getAccount(String owner)
	{
		Iterator<BankAccount> iterator = accounts.iterator();
		   while (iterator.hasNext()) {
			   BankAccount a = iterator.next();
			   if(a.getOwner().equals(owner))
			   {
			   System.out.println("Contul cautat:" +a.getOwner()+" "+a.getBalance());
			   return a;
			   }
	}
		   System.out.println("Contul cautat nu a fost gasit");
		   return null;	   
  
}
	public void getAllAccounts()
	{
		System.out.println("Conturile sortate dupa owner: ");
		Collections.sort(accounts,BankAccount.BankAccountComparator);
		Iterator<BankAccount> iterator = accounts.iterator();
	    while (iterator.hasNext()) {
	    	BankAccount a = iterator.next();
	    	System.out.println(a.getOwner() + " " + a.getBalance());
	    	
	}
}
}
