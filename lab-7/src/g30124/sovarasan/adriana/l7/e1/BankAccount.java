package g30124.sovarasan.adriana.l7.e1;

public class BankAccount {
	private String owner;
	private double balance;
	
	public BankAccount(String owner, double balance)
	{
		this.balance=balance;
		this.owner=owner;
	}
	
	public void withdraw(double amount)
	{
		
		balance -= amount;
	}
	
	public void deposit(double amount)
	{
		
		balance += amount;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		if (Double.doubleToLongBits(balance) != Double.doubleToLongBits(other.balance))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(balance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}
	
	public static void main(String[] args) {
		BankAccount b1 = new BankAccount("Alin",153);
		BankAccount b2 = new BankAccount("Alin",153);
		if(b1.equals(b2))
			System.out.println(b1+" and "+b2+ " are equals");
		else
			System.out.println(b1+" and "+b2+ " are NOT equals");
 
		if(b1.owner.equals(b2.owner))
			System.out.println(b1+" and "+b2+" have the same owners");
		else
			System.out.println(b1+" and "+b2+" have different owners");
}
}