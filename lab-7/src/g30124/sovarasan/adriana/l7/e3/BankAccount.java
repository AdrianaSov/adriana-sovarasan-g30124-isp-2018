package g30124.sovarasan.adriana.l7.e3;

import java.util.ArrayList;
import java.util.Comparator;

public class BankAccount implements Comparable<BankAccount> {
	private String owner;
	private double balance;
	
	public BankAccount(String owner, double balance)
	{
		this.balance=balance;
		this.owner=owner;
	}
	
	public void withdraw( double amount)
	{
		
		balance -= amount;
	}
	
	public void deposit(double amount)
	{
		
		balance += amount;
	}
	
	public String getOwner()
	{
		return owner;
	}
	public void setOwner(String owner) {
        this.owner = owner;
    }
	 public void setBalance(double balance) {
	        this.balance = balance;
	    }

	public double getBalance()
	{
		return balance;
	}

	public int compareTo(BankAccount o) {
        BankAccount t=BankAccount(o); 
        if(balance-o.balance < 0)
        	return -1;
        else if(balance - o.balance == 0)
        	return 0;
        else return 1;
       
	}
  
	public static Comparator<BankAccount> BankAccountComparator= new Comparator<BankAccount>() {

        public int compare(BankAccount b1, BankAccount b2) {
            
          String b1Owner = b1.getOwner().toUpperCase();
          String b2Owner = b2.getOwner().toUpperCase();
          
          return b1Owner.compareTo(b2Owner);

        }
    };
	
	private BankAccount BankAccount(BankAccount o) {
		
		return null;
	}

}