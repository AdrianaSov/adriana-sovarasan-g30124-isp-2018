package g30124.sovarasan.adriana.l7.e3;

import java.util.Comparator;
import g30124.sovarasan.adriana.l7.e3.BankAccount;


public class TreeSet_Sort_Owner implements Comparator<BankAccount>{
	
	
	public int compare(BankAccount o1, BankAccount o2) {
//		String o1Owner = o1.getOwner().toUpperCase();
//        String o2Owner = o2.getOwner().toUpperCase();
//        
//        return o1Owner.compareTo(o2Owner);
        return o1.getOwner().toUpperCase().compareTo(o2.getOwner().toUpperCase());
		
	}
	
}
