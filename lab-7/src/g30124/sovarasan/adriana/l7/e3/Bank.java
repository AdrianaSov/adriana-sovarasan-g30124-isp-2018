package g30124.sovarasan.adriana.l7.e3;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;


class Bank{
   private TreeSet<BankAccount> accounts = new TreeSet<BankAccount>();
   private TreeSet<BankAccount> accounts1 = new TreeSet<BankAccount>(new TreeSet_Sort_Owner());
    
    public void addAccount(String owner,double balance)
	{
    	accounts.add(new BankAccount(owner,balance));
    	accounts1.add(new BankAccount(owner,balance));
    	
	}
    
	public void printAccounts()
	{
		System.out.println("Conturile sortate: ");
		//Collections.sort(accounts);
		Iterator<BankAccount> iterator = accounts.iterator();
	    while (iterator.hasNext()) {
	    	BankAccount a = iterator.next();
	    	System.out.println(a.getOwner() + " " + a.getBalance());
	    }
	}
	
	public void printAccounts(double minBalance,double maxBalance)
	{
		Iterator<BankAccount> iterator = accounts.iterator();
		   while (iterator.hasNext()) {
			   BankAccount a = iterator.next();
			   if(minBalance<a.getBalance() && a.getBalance()<maxBalance)
				   System.out.println(" \n " +a.getOwner()+ " " +a.getBalance());
			   		   
		   }
           
	}
	
	public BankAccount getAccount(String owner)
	{
		Iterator<BankAccount> iterator = accounts.iterator();
		   while (iterator.hasNext()) {
			   BankAccount a = iterator.next();
			   if(a.getOwner().equals(owner))
			   {
			   System.out.println("Contul cautat:" +a.getOwner()+" "+a.getBalance());
			   return a;
			   }
	}
		   System.out.println("Contul cautat nu a fost gasit");
		   return null;	   
  
}
	public void getAllAccounts()
	{
		System.out.println("Conturile sortate dupa owner: ");
		Iterator<BankAccount> iterator = accounts1.iterator();
	    while (iterator.hasNext()) {
	    	BankAccount a = iterator.next();
	    	System.out.println(a.getOwner() + " " + a.getBalance());
	    	
	}
}
}

