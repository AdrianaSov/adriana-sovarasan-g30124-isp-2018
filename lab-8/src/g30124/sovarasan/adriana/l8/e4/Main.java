package g30124.sovarasan.adriana.l8.e4;

public class Main {
	
	
	public static void main(String[] args) throws Exception{
		CarManager cm=new CarManager();
		Car car1=cm.createCar("Golf", 400);
		Car car2=cm.createCar("Audi", 1200);
		Car car3 = cm.createCar("Renault", 800);
				
				
		cm.addCar(car1, "car1.txt");
		cm.addCar(car2, "car2.txt");
		cm.addCar(car3, "car3.txt");
		cm.removeCar("car1.txt");
		System.out.println(car1.toString());
		

	}
}

