package g30124.sovarasan.adriana.l3.ex6;


public class MyPoint {
	
	private int x, y; //Two instance variables x (int) and y (int).
	
	public MyPoint() //A �no-argument� (or �no-arg�) constructor that construct a point at (0, 0).
	{
		x=y=0;
	}
	
	public MyPoint(int x,int y)	//A constructor that constructs a point with the given x and y coordinates.
	{
		this.x=x;
		this.y=y;
	}
	
	
	//setter x
	void setX(int k){ //Getter and setter for the instance variables x and y.
    	
    		this.x=x;
    }
	
	//setter y
	void setY(int z){
		this.y=y;
		
	}
	
	//getter x
    int getx(){
    	return x;
    	
    }
    
    //getter y
    int gety(){
    	return y;
    	
    }
    
    //A method setXY() to set both x and y.
     void setXY(int x,int y){
    	this.x=x;
    	this.y=y;
    }
    //A toString() method that returns a string description of the instance in the format �(x, y)�.
    public String toString()
	{
		return "("+x+", "+y+")";
	}
    //A method called distance(int x, int y) that returns the distance from this point to another point at the given (x, y) coordinates
    public double distance(MyPoint a)
	{
		return Math.sqrt((x-a.getx())*(x-a.getx())+(y-a.gety())*(y-a.gety()));
		
	}
    
}
