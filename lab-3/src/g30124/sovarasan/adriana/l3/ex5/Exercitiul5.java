package g30124.sovarasan.adriana.l3.ex5;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;
public class Exercitiul5 {
	public static void main (String[] args) {
		City oradea = new City();
		Thing parcel = new Thing(oradea, 2, 2);
		Wall o0 = new Wall(oradea, 1, 1, Direction.WEST);
		Wall o1 = new Wall(oradea, 2, 1, Direction.WEST);
		Wall o2 = new Wall(oradea, 1, 2, Direction.EAST);
		Wall o3 = new Wall(oradea, 1, 1, Direction.NORTH);
		Wall o4 = new Wall(oradea, 1, 2, Direction.NORTH);
		Wall o5 = new Wall(oradea, 2, 1, Direction.SOUTH);
		Wall o6 = new Wall(oradea, 1, 2, Direction.SOUTH);
		
		Robot Karel = new Robot(oradea, 1, 2, Direction.SOUTH);
		
		
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.move();
		Karel.turnLeft();
		Karel.move();
		Karel.turnLeft();
		Karel.move();
		Karel.pickThing();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.move();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.move();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.move();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.turnLeft();
	   }
	}
