package g30124.sovarasan.adriana.l9.e3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Application extends JFrame {
   JTextField textField;
   JButton button;
   JTextArea textArea;
    Application ()
   {
       setTitle("Numara click-ul");
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       this.setLayout(null);
        textArea=new JTextArea();
        textArea.setBounds(70,70,200,300);
        textField=new JTextField();
        textField.setBounds(160,40,100,25);
        button=new JButton("PRESS ME!");
        button.setBounds(40,40,100,25);
        button.addActionListener(new ApasareButon());
        add(button);add(textArea);add(textField);
       setSize(500,500);
       setVisible(true);

   }
   class ApasareButon implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String text=Application.this.textField.getText();
            BufferedReader bf= null;
            try {
                bf = new BufferedReader(new FileReader(text));
                System.out.println(bf.lines());
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            try {
                text=bf.readLine();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            while(text!=null)
            {
                textArea.append(text);
                textArea.append("\n");
                try {
                    text=bf.readLine();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

       }
   }

    public static void main(String[] args) {
        new Application();
    }
}

